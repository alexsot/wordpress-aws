<?php
/** 
 * The base configurations of the WordPress.
 *
 * This file has the following configurations: MySQL settings, Table Prefix,
 * Secret Keys, WordPress Language, and ABSPATH. You can find more information by
 * visiting {@link http://codex.wordpress.org/Editing_wp-config.php Editing
 * wp-config.php} Codex page. You can get the MySQL settings from your web host.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'wordpress');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', 'root');

/** MySQL hostname */
define('DB_HOST', '172.31.36.206');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**
 * Authentication Unique Keys.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '{+=b<-#bnE!:IH{U&`HS+Y$^%uTE|d7x;#t+9>ahDIK{6o W*u=n&(=#1k6NPqwB');
define('SECURE_AUTH_KEY',  'q|T<_][}8Pow(/@u|a2lA}C>OmQ*tqMyo@+42o2qW&aCq)(:RJk4[r{/j#8Ky|BN');
define('LOGGED_IN_KEY',    ']?}+M7<wOKV:NaYI#J.{?;gUoLf$h!CxS<;aDe*kn+Tm}:,m;MC`0:MZ0:W|~:K?');
define('NONCE_KEY',        'h<cq@UWYnjUAReNKt)oH)@h=INH8r;[Bz`~alB5TrF_>[cRi{er?ZY9WaUl6>CGc');
define('AUTH_SALT',        '[e)c(<z8jN@z1 K;Ci}|=e{L|pg~rK%_y`n5YoH_ygIU-7(*lm[BiSYO>8[l<t*W');
define('SECURE_AUTH_SALT', 'p=^%yd>xfcM=GjY|JTO^^A-.~uXD2? l$4wTm;E/MVt1SgSf74d+9L-1-<_G}ZOc');
define('LOGGED_IN_SALT',   '/@X761l(&2a{zQABdzZaF7iI2`-rsX=f(}Ycti_`c2(24<k#A6t)fOC T|_m9K_a');
define('NONCE_SALT',       '2%@9uWNm&C*.&H9-dKpL=pnV9M]MiDL%YGILzhVt[U@B6+F$DlO)%>F+U,7nz|PB');




/**
 * Use external cron
 */
define('DISABLE_WP_CRON', true);

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each a unique
 * prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * WordPress Localized Language, defaults to English.
 *
 * Change this to localize WordPress.  A corresponding MO file for the chosen
 * language must be installed to wp-content/languages. For example, install
 * de.mo to wp-content/languages and set WPLANG to 'de' to enable German
 * language support.
 */
define ('WPLANG', '');

/* That's all, stop editing! Happy blogging. */

/** WordPress absolute path to the Wordpress directory. */
if ( !defined('ABSPATH') )
        define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');